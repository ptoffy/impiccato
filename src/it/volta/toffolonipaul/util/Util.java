package it.volta.toffolonipaul.util;

import java.util.Random;
import java.util.Scanner;

public class Util {

    public static int getRandomWithExclusion(Random rnd, int start, int end, int... exclude) {
        int random = start + rnd.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (random < ex) {
                break;
            }
            random++;
        }
        return random;
    }

    public static int parseInt(String value, int minValue, int maxValue, int defValue)
    {
        try {
            int iValue = Integer.parseInt(value);

            if (iValue < minValue || iValue > maxValue)
                return defValue;
            else return iValue;

        } catch (Exception e) {
            return defValue;
        }
    }

    public static float parseFloat(String value, float minValue, float maxValue, float defValue)
    {
        try {
            float iValue = Float.parseFloat(value);

            if (iValue < minValue || iValue > maxValue)
                return defValue;
            else return iValue;

        } catch (Exception e) {
            return defValue;
        }
    }

    public static String toString(char[] a) {
        if (a == null)
            return "null";
        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == iMax)
                return b.toString();
            b.append(" ");
        }
    }

    public static int readInt(Scanner scanner, String messaggio, int minValue, int maxValue, int defValue)
    {
        int result = defValue;

        while (result == defValue)
        {
            System.out.print(messaggio);
            result = parseInt(scanner.nextLine(), minValue, maxValue, defValue);
        }

        return result;
    }


}
