package it.volta.toffolonipaul.impiccato;

import it.volta.toffolonipaul.util.Util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.*;

class File {

	private List<String>    parolepossibili;         /*Lista delle parole possibili*/
	private BufferedReader  file;                    /*Reader del file txt*/
	private BufferedReader  filealfabeto;            /*Reader del file alfabeto*/
	private char[]          parola;                  /*Parola pensata dall'utente*/
	private List<String>    alfabeto;                /*Raccolta di lettere */
	private int             lunghezzaparola;         /*Variabile globale che segna la lunghezza della parola*/
	private Scanner         fl;                      /*Scanner per file*/
	private Scanner         fla;                     /*Scanner per file alfabeto*/
	private String          lettera;
	private boolean         parolaindovinata;        /*Boolean per controllare se il programma è terminato*/
	private int             contatoretentativi;      /*Conta i tentativi sbagliati*/

	File() {
		try {
			file = new BufferedReader(new InputStreamReader(new FileInputStream("src/parole.txt")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try{
			filealfabeto = new BufferedReader(new InputStreamReader(new FileInputStream("src/alfabeto.txt")));
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		parolepossibili  = new LinkedList<>();
		fl               = new Scanner(file);
		alfabeto         = new ArrayList<>();
		fla              = new Scanner(filealfabeto);
		parolaindovinata = false;
		initAlfabeto();
	}

	void tutto(){
		prendiLetteraACaso();
	}

	/*
    Crea una lista che contiene l'alfabeto da cui prendere le lettere
	 */

	private void initAlfabeto(){
		while(fla.hasNextLine()){
			String line = fla.nextLine();
			this.alfabeto.add(line);
		}
	}

	/*
    Crea una lista in cui ci sono tutte le parole con lunghezza "lunghezzaparola"
	 */

	void initListaParole(int lunghezzaparola){
		this.lunghezzaparola = lunghezzaparola;
		while (fl.hasNextLine()) {
			String line = fl.nextLine();
			if(line.length() == (lunghezzaparola))
				this.parolepossibili.add(line);
		}
		initParola();
	}

	/**
    Calcola la moda delle lettere da scegliere
	 */

	private int prendiModaLettere(){
		int[] moda = new int[26];
		for(String p : parolepossibili){
			for(char c : p.toCharArray()) {
				for (int s = 0; s < alfabeto.size(); s++) {
					for (char t : alfabeto.get(s).toCharArray()) {
						if (c == t) {
							moda[s]++;
						}
					}
				}
			}
		}
		int max = 0, modamax = 0;
		for(int c = 0; c < moda.length; c++){
			if(moda[c] > max) {
				max = moda[c];
				modamax = c;
			}
		}
		return modamax;
	}

	/**
    Aggiorna alfabeto usando lista di parole
	 */

	private void aggiornaAlfabeto(){ boolean valida;
	for(int i = 0; i < alfabeto.size(); i++){
		valida = false;
		for(String p : parolepossibili){
			if(p.contains(alfabeto.get(i))){
				valida = true;
				break;
			}

		}

		if(!valida)
		{
			alfabeto.remove(i);
			i--;
		}
	}
	}

	/**
    Prende una parola a caso e sceglie una lettera a caso
	 */

	private void prendiLetteraACaso(){
		aggiornaAlfabeto();
		String lettera = this.alfabeto.get(prendiModaLettere());
		this.alfabeto.remove(lettera);
		setLettera(lettera);
	}

	/**
    Inizializza la parola
	 */

	private void initParola(){
		this.parola = new char[lunghezzaparola];
		for(int i = 0; i < lunghezzaparola; i++)                        //Inizializza con '.' ogni cella della parola
			this.parola[i] = '.';
	}

	/**
    Mette la lettera "lettera" in posizione "posizione" della parola
	 */

	void mettiLettereInParola(int posizione){
		this.parola[posizione] = this.lettera.charAt(0);                //Mette la lettera
	}

	/**
    Rimuove dalla lista le parole che non contengono la lettera "lettera" in posizione "posizione"
	 */

	void rimuoviParolaRispostaNo(String lettera){
		for (int i = 0; i < parolepossibili.size(); i++){
			if(parolepossibili.get(i).contains(lettera)) {
				this.parolepossibili.remove(i);
				i--;
			}
		}
	}

	/**
    Se la parola è stata indovinata, ritorna true
	 */

	void controllaParolaIndovinata(){
		String parola = String.valueOf(this.parola);
		if(!parola.contains("."))
			this.parolaindovinata = true;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------


	boolean isParolaindovinata() {
		return parolaindovinata;
	}

	private void setLettera(String lettera){
		this.lettera = lettera;
	}

	List<String> getParolepossibili(){
		return parolepossibili;
	}

	String getLettera() {
		return lettera;
	}

	String toStringParola() {
		return Util.toString(parola);
	}

	String getParola() {
		return String.valueOf(this.parola);
	}
	
	void setParola(String parola) {
		this.parola = parola.toCharArray();
	}

	int getContatoretentativi() {
		return contatoretentativi;
	}

	void setContatoretentativi(int contatoretentativi) {
		this.contatoretentativi = contatoretentativi;
	}

	void setPosizione(int posizione) {
		int posizione1 = posizione;
	}

	void aggiornaListaParolePossibili(int posizione) {
		for(int c = 0; c < parolepossibili.size(); c++) {
			char[] parola = parolepossibili.get(c).toCharArray();
			if(parola[posizione] != lettera.charAt(0)) {
				parolepossibili.remove(c);
				c--;
			}

		}
	}

	void rimuoviParola(String parola2) {
		parolepossibili.remove(parola2);
	}

    void setParolaindovinata(boolean b) {
        this.parolaindovinata = b;
    }
}
