package it.volta.toffolonipaul.impiccato;

import it.volta.toffolonipaul.util.Util;

import java.util.Scanner;

class Gestione {

	private Scanner s;
	private File f;

	Gestione() {
		s = new Scanner(System.in);
		f = new File();
	}

	void tutto(){
		numeroLettere();
		mostraParola();
		while(!f.isParolaindovinata()) {
			f.tutto();
			chiediLetteraInPosizione();
			mostraParola();
			f.controllaParolaIndovinata();
		}
		haIndovinatoParola();
	}

	private void numeroLettere(){
		System.out.println("Inserisci il numero di lettere della parola");
		String nlettere = s.nextLine();
		int numerolettere = Util.parseInt(nlettere, 1, 15, 4);
		this.f.initListaParole(numerolettere);
	}

	private void haIndovinatoParola(){
		System.out.println("La tua parola è: " + f.getParola());
		System.out.println("L'ho indovinata sbagliando " + f.getContatoretentativi() + " tentativi!");
	}

	private void chiediLetteraInPosizione(){
		String lettera = f.getLettera();
		System.out.println("Nella tua parola c'è la lettera " + lettera + "?");
		String risposta = s.nextLine().toLowerCase();

		if(risposta.equals("si")) {
			this.rispostaPositiva();
			System.out.println("Ce ne sono altre?");
			String risposta2 = s.nextLine();
			while(risposta2.toLowerCase().equals("si")) {
				this.rispostaPositiva();
				System.out.println("Ce ne sono altre?");
				risposta2 = s.nextLine();
				risposta2.toLowerCase();
			}
			if(f.getParolepossibili().size() <= 10)
				proponiParola();
		}
		else
		{
			f.rimuoviParolaRispostaNo(f.getLettera().trim());
			this.f.setContatoretentativi(f.getContatoretentativi() + 1);
		}
	}

	private void rispostaPositiva(){
		System.out.println("In che posizione?");
		String pos = s.nextLine();
		int posizione = Util.parseInt(pos,0,15,3) -1;
		this.f.mettiLettereInParola(posizione);
		this.f.setPosizione(posizione);

		f.aggiornaListaParolePossibili(posizione);
	}

	private void proponiParola(){
		
		int random = (int) (Math.random()*f.getParolepossibili().size());
		String parola = f.getParolepossibili().get(random);
		
		System.out.println("La tua parola è forse " + parola + "?");
		String risposta = s.nextLine();
		if(risposta.equals("si") || risposta.equals("s")) {
			this.f.setParolaindovinata(true);
			f.setParola(parola);
		}
		else {
			f.rimuoviParola(parola);
			this.f.setContatoretentativi(f.getContatoretentativi() + 1);
		}
	}

	private void mostraParola(){
		System.out.println(f.toStringParola());
	}

	String getParola(){
		return f.getParola();
	}

	public String hoIndovinatoLaParola(){
		return "La tua parola è: " + f.getParola();
	}

	public String propongoLettera(){
		return "Nella tua parola c'è la lettera " + f.getLettera() + "?";
	}
}
