package it.volta.toffolonipaul.impiccato;

import it.volta.toffolonipaul.util.Util;
import javafx.scene.control.TextInputDialog;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ImpiccatoGUI extends JFrame {

    private JLabel parola, domanda;
    private JTextField input;
    private File f;
    private String nlettere;
    private JButton si, no;
    private Ascoltatore asc;

    public ImpiccatoGUI(String title, Gestione gestione, File f) {
        super(title);
        this.f = f;
        this.setSize(1440, 900);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
        this.getContentPane().setLayout(null);

        parola = new JLabel("Ciao");
        parola.setBounds(30, 20, 300, 50);
        this.getContentPane().add(parola);
        parola.setVisible(true);

        domanda = new JLabel("Inserisci il numero di lettere della parola: ");
        domanda.setBounds(30, 150, 300, 50);
        this.getContentPane().add(domanda);

        input = new JTextField();
        input.setBounds(330, 165, 40, 25);
        this.getContentPane().add(input);

        si = new JButton("Sì!");
        si.addActionListener(asc);
        no = new JButton("No!");
        no.addActionListener(asc);
        si.setBounds(30, 300, 100, 100);
        no.setBounds(150, 300, 100, 100);
        this.add(si);
        this.add(no);
    }

//    void tutto(){
//        numeroLettere();
//        while(!f.isParolaindovinata()) {
//            f.tutto();
//            chiediLetteraInPosizione();
//            mostraParola();
//            f.controllaParolaIndovinata();
//        }
//        haIndovinatoParola();
//    }
//
//    private void numeroLettere(){
//        nlettere = input.getText();
//        this.f.initListaParole(nlettere);
//    }
//
//    private void chiediLetteraInPosizione(){
//        String lettera = f.getLettera();
//        domanda.setText("Nella tua parola c'è la lettera " + lettera + "?");
//        String risposta = input.getText().toLowerCase();
//
//        if(risposta.equals("si")) {
//            this.rispostaPositiva();
//            System.out.println("Ce ne sono altre?");
//            String risposta2 = s.nextLine();
//            while(risposta2.toLowerCase().equals("si")) {
//                this.rispostaPositiva();
//                System.out.println("Ce ne sono altre?");
//                risposta2 = s.nextLine();
//                risposta2.toLowerCase();
//            }
//            if(f.getParolepossibili().size() <= 10)
//                proponiParola();
//        }
//        else
//        {
//            f.rimuoviParolaRispostaNo(f.getLettera().trim());
//            this.f.setContatoretentativi(f.getContatoretentativi() + 1);
//        }
//    }
}

